alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../../"

alias c="tr -d '\n' | pbcopy"

alias cleanup="find . -type f -name '*.DS_Store' -ls -delete"

alias hidedesktop="defaults write com.apple.finder CreateDesktop -bool false && killall Finder"
alias showdesktop="defaults write com.apple.finder CreateDesktop -bool true && killall Finder"

alias map="xargs -n1"

alias path='echo -e ${PATH//:/\\n}'

