local function get_config(name)
  return string.format('require("plugins/%s")', name)
end

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

local packer = require('packer')

packer.init({
  enable = true,
  threshold = 0,
  max_jobs = 20,
  display = {
    open_fn = function()
      return require('packer.util').float()
    end,
  },
})

packer.startup(function(use)
  -- Packer can manage itself
  use({ 'wbthomason/packer.nvim' })

  -- Plugins
  use({
    'nvim-tree/nvim-tree.lua',
    requires = {
      'nvim-tree/nvim-web-devicons',
    },
    config = get_config("tree"),
  })

 -- COC
  use({
    'neoclide/coc.nvim',
    branch = 'release',
  })
end)

