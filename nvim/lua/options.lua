vim.g.python3_host_prog = '/home/curtis/dev/libs/neovim-venv/bin/python3.10'

vim.opt.showmatch = true
vim.opt.ignorecase = true
vim.opt.mouse = "v"
vim.opt.hlsearch = true
vim.opt.incsearch = true
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.expandtab = true
vim.opt.shiftwidth = 2
vim.opt.autoindent = true
vim.opt.number = true
vim.opt.wildmode = "longest,list"
vim.opt.cc = {80}
vim.opt.mouse = "a"
vim.opt.cursorline = true
vim.opt.ttyfast = true
vim.opt.wrap = false
vim.opt.termguicolors = true

